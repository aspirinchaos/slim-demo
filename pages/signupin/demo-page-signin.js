import { TemplateController } from 'meteor/template-controller';
import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-page-signin.html';

TemplateController('demoPageSignin', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    pathUp() {
      return FlowRouter.path('sign-simple-up');
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
