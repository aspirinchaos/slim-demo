import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-datepicker.html';

TemplateController('demoDatepicker', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    picker() {
      return {
        color: 'primary',
        onSelect: (text) => {
          console.log(text);
        },
        groupClassName: 'w-25',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
