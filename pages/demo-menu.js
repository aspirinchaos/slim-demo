import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-menu.html';

TemplateController('demoMenu', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    menu() {
      return [
        { name: 'items', value: 'Массив с элементами меню, Array' },
        { name: 'items.title', value: 'Заголовок, String' },
        { name: 'items.icon', value: 'Иконка, обычный html, String' },
        { name: 'items.after', value: 'Html контент для вывода после заголовка, String' },
        { name: 'items.mega', value: 'Html контент для мега меню, String' },
        {
          name: 'items.route',
          value: 'Имя/имена роутов для определения активности меню. Массив используется только для мега меню, String | [String] ',
        },
        { name: 'items.subitems', value: 'Подэлементы меню, Array' },
        { name: 'items.subitems.title', value: 'Заголовок, String' },
        { name: 'items.subitems.route', value: 'Имя роута' },
        { name: 'items.subitems.subitems', value: 'Второй уровень подэлементов меню, Array' },
        { name: 'items.subitems.subitems.title', value: 'Заголовок, String' },
        { name: 'items.subitems.subitems.route', value: 'Имя роута' },
      ];
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
