import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './bs-modal.html';

TemplateController('bsModal', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    myButton() {
      return {
        content: 'Open Modal',
        onClick: () => {
          this.openModal();
        },
      };
    },
    myModal() {
      return {
        title: 'Modal title',
        /**
         * Получения методов для работы с модальным окном
         * в колбэк приходит объект!
         * @param modal jQuery объект модального окна
         * @param show метод для открытия окна
         * @param hide метод для закрытия окна
         */
        handleModal: ({ modal, show, hide }) => {
          this.openModal = show;
          this.closeModal = hide;
        },
      };
    },
    close() {
      return {
        content: 'Close modal',
        color: 'secondary',
        // закрывать модальное можно воспользовавшись атрибутом
        attributes: {
          'data-dismiss': 'modal',
        },
      };
    },
    save() {
      return {
        content: 'Save changes',
        color: 'primary',
        onClick: () => {
          this.closeModal();
        },
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    openModal: null,
    closeModal: null,
  },
});
