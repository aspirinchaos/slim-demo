import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './bs-grid.html';
import './bs-page-header';

TemplateController('bsGrid', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    rowList() {
      return [
        { name: 'noGutters', value: 'true' },
        { name: 'horizontal', value: 'start | center | end | around | between' },
        { name: 'vertical', value: 'start | center | end | baseline | stretch' },
        { name: 'xs', value: this.object('horizontal, vertical') },
        { name: 'sm', value: this.object('horizontal, vertical') },
        { name: 'md', value: this.object('horizontal, vertical') },
        { name: 'lg', value: this.object('horizontal, vertical') },
        { name: 'xl', value: this.object('horizontal, vertical') },
      ];
    },
    colList() {
      return [
        { name: 'size', value: '1..12 | auto | true' },
        { name: 'order', value: '0..12 | first | last' },
        { name: 'offset', value: '0..12' },
        { name: 'vertical', value: 'start | center | end | baseline | stretch' },
        {
          name: 'xs',
          value: `1..12 | auto | true | ${this.object('size, order, offset, vertical')}`,
        },
        {
          name: 'sm',
          value: `1..12 | auto | true | ${this.object('size, order, offset, vertical')}`,
        },
        {
          name: 'md',
          value: `1..12 | auto | true | ${this.object('size, order, offset, vertical')}`,
        },
        {
          name: 'lg',
          value: `1..12 | auto | true | ${this.object('size, order, offset, vertical')}`,
        },
        {
          name: 'xl',
          value: `1..12 | auto | true | ${this.object('size, order, offset, vertical')}`,
        },
      ];
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    object(text) {
      return `{ <code class="pd-0">${text}</code> }`;
    },
  },
});
