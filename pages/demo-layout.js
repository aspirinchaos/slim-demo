import { TemplateController } from 'meteor/template-controller';
import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-layout.html';

TemplateController('demoLayout', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    errorLinks() {
      return [
        { path: FlowRouter.path('error-404'), title: 'Ошибка 404' },
        { path: FlowRouter.path('error-500'), title: 'Ошибка 500' },
        { path: FlowRouter.path('error-503'), title: 'Ошибка 503' },
        { path: FlowRouter.path('error-505'), title: 'Ошибка 505' },
      ];
    },
    simpleLinks() {
      return [
        { path: FlowRouter.path('sign-simple-in'), title: 'Страница логина' },
        { path: FlowRouter.path('sign-simple-up'), title: 'Страница регистрации' },
      ];
    },
    splitLinks() {
      return [
        { path: FlowRouter.path('sign-split-in'), title: 'Страница логина' },
        { path: FlowRouter.path('sign-split-up'), title: 'Страница регистрации' },
      ];
    },
    mainList() {
      return [
        {
          name: 'header',
          value: 'Имя тимплейта для одиночного хедера. <b>Использовать либо цельный, либо разделенный</b>',
        },
        { name: 'headerLeft', value: 'Имя тимплейта для левой стороны хедера' },
        { name: 'headerRight', value: 'Имя тимплейта для правой стороны хедера' },
        { name: 'navbar', value: 'Имя тимплейта для меню' },
        { name: 'template', value: 'Имя тимплейта для оснвной части' },
        { name: 'footer', value: 'Имя тимплейта для футера' },
      ];
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
