import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-russian-city.html';

TemplateController('demoRussianCity', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {
    city: 'Не выбран',
    name: '',
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    getCity() {
      return {
        onChange: (cityId, cityName) => {
          this.state.city = cityId;
          this.state.name = cityName;
        },
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
