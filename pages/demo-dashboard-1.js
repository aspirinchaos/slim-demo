import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-dashboard-1.html';

TemplateController('demoDashboard1', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    chartArea1() {
      return {
        className: 'dash-chartist',
        type: 'line',
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [
          [6, 8, 7, 10, 14, 11, 16, 18],
          [2, 4, 3, 4, 5, 3, 5, 4],
        ],
        options: {
          high: 30,
          low: 0,
          axisY: {
            onlyInteger: true,
            offset: 0,
          },
          axisX: {
            offset: 0,
          },
          showArea: true,
          fullWidth: true,
          chartPadding: {
            bottom: 0,
            left: 0,
            right: 0,
            top: 0,
          },
        },
      };
    },
    chartBar1() {
      return {
        className: 'chart-rickshaw',
        type: 'bar',
        stack: false,
        max: 60,
        series: [
          {
            data: [
              { x: 0, y: 20 },
              { x: 1, y: 25 },
              { x: 2, y: 10 },
              { x: 3, y: 20 },
              { x: 4, y: 15 },
              { x: 5, y: 18 },
              { x: 6, y: 15 },
              { x: 7, y: 3 },
              { x: 8, y: 2 },
              { x: 9, y: 5 },
              { x: 10, y: 3 },
              { x: 11, y: 2 },
              { x: 12, y: 4 },
              { x: 13, y: 5 },
              { x: 14, y: 1 },
              { x: 15, y: 2 },
            ],
            color: '#8AC6E8',
          },
          {
            data: [
              { x: 0, y: 10 },
              { x: 1, y: 30 },
              { x: 2, y: 45 },
              { x: 3, y: 30 },
              { x: 4, y: 25 },
              { x: 5, y: 15 },
              { x: 6, y: 10 },
              { x: 7, y: 4 },
              { x: 8, y: 3 },
              { x: 9, y: 2 },
              { x: 10, y: 5 },
              { x: 11, y: 2 },
              { x: 12, y: 3 },
              { x: 13, y: 2 },
              { x: 14, y: 4 },
              { x: 15, y: 5 },
            ],
            color: '#1B84E7',
          },
        ],
      };
    },
    sl3() {
      return {
        className: 'sparkline wd-100p',
        values: [1, 4, 4, 7, 5, 9, 10, 5, 4, 4, 7, 5, 9, 10],
        type: 'line',
        options: {
          width: '100%',
          height: '45',
          lineColor: '#0083CD',
          fillColor: 'rgba(0,131,205,0.2)',
        },
      };
    },
    sl4() {
      return {
        className: 'sparkline wd-100p',
        values: [1, 4, 4, 7, 5, 7, 4, 3, 4, 4, 6, 5, 9, 7],
        type: 'line',
        options: {
          width: '100%',
          height: '45',
          lineColor: '#0D0D6B',
          fillColor: 'rgba(13,13,107,0.2)',
        },
      };
    },
    chartLine1() {
      return {
        className: 'ht-50 ht-sm-70',
        type: 'line',
        series: [{
          data: [
            { x: 0, y: 5 },
            { x: 1, y: 7 },
            { x: 2, y: 10 },
            { x: 3, y: 11 },
            { x: 4, y: 12 },
            { x: 5, y: 10 },
            { x: 6, y: 9 },
            { x: 7, y: 7 },
            { x: 8, y: 6 },
            { x: 9, y: 8 },
            { x: 10, y: 9 },
            { x: 11, y: 10 },
            { x: 12, y: 7 },
            { x: 13, y: 10 },
          ],
          color: '#1B84E7',
        }],
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
