import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import { Popup } from 'meteor/sweetalert';
import { Toastr } from 'meteor/toastr';
// import template
import './demo-popup.html';

TemplateController('demoPopup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    showWarning() {
      return {
        color: 'warning',
        content: 'Показать warning Toastr',
        onClick: () => {
          Toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!');
        },
      };
    },
    showSuccess() {
      return {
        color: 'success',
        content: 'Показать success Toastr',
        onClick: () => {
          Toastr.success('Have fun storming the castle!', 'Miracle Max Says');
        },
      };
    },
    showError() {
      return {
        color: 'danger',
        content: 'Показать error Toastr',
        onClick: () => {
          Toastr.error('I do not think that word means what you think it means.', 'Inconceivable!');
        },
      };
    },
    showInfo() {
      return {
        color: 'info',
        content: 'Показать info Toastr',
        onClick: () => {
          Toastr.info('We do have the Kapua suite available.', 'Turtle Bay Resort');
        },
      };
    },
    clearToastr() {
      return {
        color: 'primary',
        content: 'Убрать Toastr',
        onClick: () => {
          Toastr.clear();
        },
      };
    },
    popupConfirm() {
      return {
        content: 'Показать confirm Popup',
        color: 'warning',
        onClick: () => {
          Popup.confirm({
            title: 'Are you sure?',
            text: 'You will not be able to recover this imaginary file!',
            type: 'warning',
          }).then((result) => {
            if (result.value) {
              Popup.Swal('Deleted!', 'Your file has been deleted.', 'success');
            } else if (result.dismiss === Popup.Swal.DismissReason.cancel) {
              Popup.Swal('Cancelled', 'Your imaginary file is safe :)', 'error');
            }
          });
        },
      };
    },
    popupSuccess() {
      return {
        content: 'Показать success Popup',
        color: 'success',
        onClick: () => {
          Popup.success('Good job!', 'You clicked the button!');
        },
      };
    },
    popupError() {
      return {
        color: 'danger',
        content: 'Показать error Popup',
        onClick: () => {
          Popup.error({ title: 'Oops...', text: 'Something went wrong!' });
        },
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
