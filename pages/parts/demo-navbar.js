import { TemplateController } from 'meteor/template-controller';
import { IonIcon } from 'meteor/ionicons';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-navbar.html';
// import components
import './demo-mega';

TemplateController('demoNavbar', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    menu() {
      return {
        items: [
          {
            title: 'Dashboard',
            icon: IonIcon({ name: 'home' }),
            subitems: [
              { title: 'Dashboard 01', route: 'demo-dashboard-1' },
              { title: 'Dashboard 02', route: 'demo-dashboard-2' },
              { title: 'Dashboard 03', route: 'demo-dashboard-3' },
              { title: 'Dashboard 04', route: 'demo-dashboard-4' },
              { title: 'Dashboard 05', route: 'demo-dashboard-5' },
            ],
          },
          {
            title: 'UI Elements',
            icon: IonIcon({ name: 'filing' }),
            mega: 'demoMega',
            route: ['demo-concept', 'demo-layout', 'demo-menu', 'demo-popup'],
          },
          {
            title: 'Bootstrap',
            icon: IonIcon({ name: 'cube' }),
            subitems: [
              { title: 'Grid система', route: 'bs-grid' },
              { title: 'Card компоненты', route: 'bs-card' },
              { title: 'Button компонент', route: 'bs-button' },
              { title: 'Alert компонент', route: 'bs-alert' },
              { title: 'Modal компонент', route: 'bs-modal' },
              { title: 'Tooltip компонент', route: 'bs-tooltip' },
              { title: 'List group компонент', route: 'bs-list-group' },
            ],
          },
          {
            title: 'Pages',
            icon: IonIcon({ name: 'book' }),
            subitems: [
              { title: 'Profile Page' },
              { title: 'Invoice' },
              { title: 'Contact Manager' },
              { title: 'File Manager' },
              { title: 'Calendar' },
              { title: 'Timeline' },
              {
                title: 'Pricing',
                subitems: [
                  { title: 'Pricing 01' },
                  { title: 'Pricing 02' },
                  { title: 'Pricing 03' },
                ],
              },
              {
                title: 'Логин',
                subitems: [
                  { title: 'Логин простой', route: 'sign-simple-in' },
                  { title: 'Логин двойной', route: 'sign-split-in' },
                ],
              },
              {
                title: 'Регистрация',
                subitems: [
                  { title: 'Регистрация простая', route: 'sign-simple-up' },
                  { title: 'Регистрация двойная', route: 'sign-split-in' },
                ],
              },
              {
                title: 'Страницы ошибок',
                subitems: [
                  { title: 'Ошибка 404 Not Found', route: 'error-404' },
                  { title: 'Ошибка 505 Forbidden', route: 'error-505' },
                  { title: 'Ошибка 500 Internal Server', route: 'error-500' },
                  { title: 'Ошибка 503 Service Unavailable', route: 'error-504' },
                ],
              },
            ],
          },
          {
            title: 'Forms',
            icon: IonIcon({ name: 'cog' }),
            subitems: [
              { title: 'Form Elements' },
              { title: 'Form Layouts' },
              { title: 'Form Validation' },
              { title: 'Form Wizards' },
              { title: 'WYSIWYG Editor' },
              { title: 'Select2' },
              { title: 'Range Slider' },
              { title: 'Datepicker' },
            ],
          },
          {
            title: 'Messages',
            icon: IonIcon({ name: 'chatboxes' }),
            after: '<span class="square-8"></span>',
          },
          {
            title: 'Widgets',
            icon: IonIcon({ name: 'analytics' }),
          },
        ],
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
