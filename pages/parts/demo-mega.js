import { TemplateController } from 'meteor/template-controller';
import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';
// import template
import './demo-mega.html';

TemplateController('demoMega', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    leftComponents() {
      return [
        { path: FlowRouter.path('demo-concept'), title: 'Концепция компонентов' },
        { path: FlowRouter.path('demo-layout'), title: 'Лайоуты' },
        { path: FlowRouter.path('demo-menu'), title: 'Меню' },
        { path: FlowRouter.path('demo-popup'), title: 'Уведомления' },
      ];
    },
    rightComponents() {
      return [
        { path: FlowRouter.path('demo-russian-city'), title: 'Выбор города' },
        { path: FlowRouter.path('demo-send-sms'), title: 'Отправка смс' },
        { path: FlowRouter.path('demo-uploader'), title: 'Загрузка файлов' },
        { path: FlowRouter.path('demo-datepicker'), title: 'Выбор даты' },
      ];
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
