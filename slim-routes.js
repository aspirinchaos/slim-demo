import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
// pages
import './pages/demo-home';
import './pages/demo-dashboard-1';
import './pages/demo-datepicker';
import './pages/demo-concept';
import './pages/demo-layout';
import './pages/demo-menu';
import './pages/demo-popup';
import './pages/demo-russian-city';
import './pages/demo-send-sms';
import './pages/demo-uploader';
// parts
import './pages/parts/demo-navbar';
import './pages/parts/demo-header-left';
import './pages/parts/demo-header-right';
import './pages/parts/demo-footer';

const fullPage = {
  navbar: 'demoNavbar',
  headerLeft: 'demoHeaderLeft',
  headerRight: 'demoHeaderRight',
  footer: 'demoFooter',
};

const demoRoutes = FlowRouter.group({
  prefix: '/demo',
  name: 'demo',
});

demoRoutes.route('/', {
  name: 'demo-home',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoHome', ...fullPage });
  },
});

demoRoutes.route('/dashboard-1', {
  name: 'demo-dashboard-1',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoDashboard1', ...fullPage });
  },
});

demoRoutes.route('/concept', {
  name: 'demo-concept',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoConcept', ...fullPage });
  },
});

demoRoutes.route('/layout', {
  name: 'demo-layout',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoLayout', ...fullPage });
  },
});

demoRoutes.route('/menu', {
  name: 'demo-menu',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoMenu', ...fullPage });
  },
});

demoRoutes.route('/popup', {
  name: 'demo-popup',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoPopup', ...fullPage });
  },
});

demoRoutes.route('/russian-city', {
  name: 'demo-russian-city',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoRussianCity', ...fullPage });
  },
});

demoRoutes.route('/send-sms', {
  name: 'demo-send-sms',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoSendSms', ...fullPage });
  },
});

demoRoutes.route('/uploader', {
  name: 'demo-uploader',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoUploader', ...fullPage });
  },
});

demoRoutes.route('/datepicker', {
  name: 'demo-datepicker',
  action() {
    BlazeLayout.render('MainLayout', { template: 'demoDatepicker', ...fullPage });
  },
});

export { fullPage, demoRoutes };
