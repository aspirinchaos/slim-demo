// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by slim-demo.js.
import { name as packageName } from "meteor/slim-demo";

// Write your tests here!
// Here is an example.
Tinytest.add('slim-demo - example', function (test) {
  test.equal(packageName, "slim-demo");
});
