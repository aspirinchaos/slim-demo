Package.describe({
  name: 'slim-demo',
  version: '0.1.6',
  summary: 'Slim template demo and documentation',
  git: 'https://bitbucket.org/aspirinchaos/slim-demo.git',
  documentation: 'README.md',
  // development only
  // debugOnly: true,
});

Npm.depends({
  prismjs: '1.15.0',
});

Package.onUse(function (api) {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'ostrio:meteor-root',
    'kadira:flow-router',
    'kadira:blaze-layout',
    'templating',
    'template-controller',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'rickshaw',
    'chartist',
    'sparkline',
    'font-awesome',
    'ionicons',
    'sweetalert',
    'toastr',
    'russian-city',
    'slim',
  ]);
  // public
  api.addAssets([
    'public/icon1.svg',
    'public/icon2.svg',
  ], 'client');
  // code highlight
  api.addFiles([
    'code/code.js',
    'code/Blaze-code.js',
    'code/JS-code.js',
    'code/table-reference.js',
    'code/style/core/prism-tomorrow.css',
    'code/style/prism-toolbar.css',
    'code/style/prism-line-numbers.css',
    'code/style/prism-additional.css',
  ], 'client');
  // bs examples
  api.addFiles([
    'code/style/bs-additional.css',
  ], 'client');
  api.addFiles([
    'slim-routes.js',
    'bs-routes.js',
    'error-routes.js',
    'signupin-routes.js',
  ], 'client');
  api.mainModule('slim-demo.js', 'server');
});

Package.onTest(function (api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('slim-demo');
  api.mainModule('slim-demo-tests.js');
});
