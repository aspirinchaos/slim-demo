import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { fullPage, demoRoutes } from './slim-routes';
// pages
import './pages/bs/bs-grid';
import './pages/bs/bs-card';
import './pages/bs/bs-button';
import './pages/bs/bs-alert';
import './pages/bs/bs-modal';
import './pages/bs/bs-tooltip';

const bsRoutes = demoRoutes.group({
  prefix: '/bs',
  name: 'bs',
});

bsRoutes.route('/', {
  name: 'bs-home',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsHome', ...fullPage });
  },
});

bsRoutes.route('/grid', {
  name: 'bs-grid',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsGrid', ...fullPage });
  },
});

bsRoutes.route('/card', {
  name: 'bs-card',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsCard', ...fullPage });
  },
});

bsRoutes.route('/button', {
  name: 'bs-button',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsButton', ...fullPage });
  },
});

bsRoutes.route('/alert', {
  name: 'bs-alert',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsAlert', ...fullPage });
  },
});

bsRoutes.route('/modal', {
  name: 'bs-modal',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsModal', ...fullPage });
  },
});

bsRoutes.route('/tooltip', {
  name: 'bs-tooltip',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsTooltip', ...fullPage });
  },
});

bsRoutes.route('/list-group', {
  name: 'bs-list-group',
  action() {
    BlazeLayout.render('MainLayout', { template: 'bsListGroup', ...fullPage });
  },
});
