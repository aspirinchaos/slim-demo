import Prism from 'prismjs';
import ClipboardJS from 'clipboard';
import { Template } from 'meteor/templating';
import 'prismjs/components/prism-markup-templating';
import 'prismjs/components/prism-handlebars';
import 'prismjs/plugins/toolbar/prism-toolbar';
import 'prismjs/plugins/line-numbers/prism-line-numbers';

// Создадим кнопку для копирования
Prism.plugins.toolbar.registerButton('copy-to-clipboard', function (env) {
  const linkCopy = document.createElement('a');
  linkCopy.textContent = 'Копировать';
  const resetText = () => setTimeout(() => {
    linkCopy.textContent = 'Копировать';
  }, 5000);

  function registerClipboard() {
    const clip = new ClipboardJS(linkCopy, {
      text() {
        return env.code;
      },
    });

    clip.on('success', function () {
      linkCopy.textContent = 'Скопирован!';
      resetText();
    });
    clip.on('error', function () {
      linkCopy.textContent = 'Нажмите Ctrl+C для копирования';
      resetText();
    });
  }

  registerClipboard();
  return linkCopy;
});

/**
 * хелперы для отображения spacebars кода в тимплейте, иначе все компилится в тимплейт
 * а так является прострой строкой
 * HTML комментарий
 */
Template.registerHelper('_c', text => `<!--${text}-->`);
// вывод html
Template.registerHelper('_ht', text => `${text}`);
// обычный хелпер
Template.registerHelper('_h', text => `{{${text}}}`);
// вставка шаблона
Template.registerHelper('_i', text => `{{>${text}}}`);
// начало блока хелпера
Template.registerHelper('_s', text => `{{#${text}}}`);
// начало мультилайна блока хелпера
Template.registerHelper('_sm', text => `{{#${text}`);
// конец блока хелпера
Template.registerHelper('_e', text => `{{/${text}}}`);
