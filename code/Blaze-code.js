import { TemplateController } from 'meteor/template-controller';
import { HTML } from 'meteor/htmljs';
import Prism from 'prismjs';
import Normalizer from 'prismjs/plugins/normalize-whitespace/prism-normalize-whitespace';
// import template
import './Blaze-code.html';

TemplateController('BlazeCode', {
  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.content = Blaze._toText(this.view.templateContentBlock, HTML.TEXTMODE.STRING);
    this.content = this.content.replace(/^\n|\n\s*$/g, '');
  },
  onRendered() {
    const pre = document.createElement('pre');
    const code = document.createElement('code');
    this.firstNode.appendChild(pre);
    pre.appendChild(code);
    code.className = 'language-handlebars line-numbers';
    code.textContent = this.nw.normalize(this.content);
    Prism.highlightElement(code);
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    content: null,
    nw: new Normalizer({
      'remove-trailing': true,
      'remove-indent': true,
      'left-trim': true,
      'right-trim': true,
    }),
  },
});
