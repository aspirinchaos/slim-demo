import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { demoRoutes } from './slim-routes';
// pages
import './pages/signupin/demo-page-signin';
import './pages/signupin/demo-page-signup';
import './pages/signupin/demo-signin-right';
import './pages/signupin/demo-signin-left';
import './pages/signupin/demo-signup-right';
import './pages/signupin/demo-signup-left';

const signRoutes = demoRoutes.group({
  prefix: '/sign',
  name: 'sign',
});

signRoutes.route('/simple-in', {
  name: 'sign-simple-in',
  action() {
    BlazeLayout.render('SimpleLayout', { template: 'demoPageSignin' });
  },
});

signRoutes.route('/simple-up', {
  name: 'sign-simple-up',
  action() {
    BlazeLayout.render('SimpleLayout', { template: 'demoPageSignup' });
  },
});

signRoutes.route('/split-in', {
  name: 'sign-split-in',
  action() {
    BlazeLayout.render('SplitLayout', { left: 'demoSigninLeft', right: 'demoSigninRight' });
  },
});

signRoutes.route('/split-up', {
  name: 'sign-split-up',
  action() {
    BlazeLayout.render('SplitLayout', { left: 'demoSignupLeft', right: 'demoSignupRight' });
  },
});
