import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import serveStatic from 'serve-static';

checkNpmVersions({
  'serve-static': '1.13.2',
}, 'slim-demo');

Meteor.startup(() => {
  let path = `${Meteor.absolutePath}/packages/slim-demo/template/app`;
  if (Meteor.isProduction) {
    path = `${process.cwd()}/assets/app/slim/app`;
  }
  const serve = serveStatic(
    path,
    { index: ['index.html'] },
  );
  WebApp.rawConnectHandlers.use('/slim-demo-orig', (req, res, next) => {
    serve(req, res, next);
  });
});
