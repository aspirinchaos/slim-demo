import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { demoRoutes } from './slim-routes';
// pages
import './pages/errors/demo-page-404.html';
import './pages/errors/demo-page-500.html';
import './pages/errors/demo-page-503.html';
import './pages/errors/demo-page-505.html';

const errorRoutes = demoRoutes.group({
  prefix: '/error',
  name: 'error',
});

errorRoutes.route('/404', {
  name: 'error-404',
  action() {
    BlazeLayout.render('ErrorLayout', { template: 'demoPage404' });
  },
});

errorRoutes.route('/500', {
  name: 'error-500',
  action() {
    BlazeLayout.render('ErrorLayout', { template: 'demoPage500' });
  },
});

errorRoutes.route('/503', {
  name: 'error-503',
  action() {
    BlazeLayout.render('ErrorLayout', { template: 'demoPage503' });
  },
});

errorRoutes.route('/505', {
  name: 'error-505',
  action() {
    BlazeLayout.render('ErrorLayout', { template: 'demoPage505' });
  },
});
